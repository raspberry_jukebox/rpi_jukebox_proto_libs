#pragma once
#include "rpi_jukebox/api/rpi_jukebox_events.pb.h"

namespace raspberry_jukebox {

constexpr inline const char* get_action_name_for_id(rpi_jukebox_events::ACTION action) {
	switch(action) {
	case rpi_jukebox_events::ACTION::ACTION_FORWARD:
		return "FORWARD";
	case rpi_jukebox_events::ACTION::ACTION_PAUSE:
		return "PAUSE";
	case rpi_jukebox_events::ACTION::ACTION_PLAY:
		return "PLAY";
	case rpi_jukebox_events::ACTION::ACTION_REWIND:
		return "REWIND";
	case rpi_jukebox_events::ACTION::ACTION_STOP:
		return "STOP";
	default:
		return "UNKNOWN ACTION";
	}
}

}
