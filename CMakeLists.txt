cmake_minimum_required(VERSION 3.1.3)
project(rpi_jukebox_proto_libs)

find_package(Git)

execute_process(COMMAND ${GIT_EXECUTABLE} --git-dir=${CMAKE_CURRENT_SOURCE_DIR}/.git describe --tags --always --dirty RESULT_VARIABLE GIT_RESULT
    OUTPUT_VARIABLE GIT_VERSION)

string(REGEX REPLACE "\n$" "" GIT_VERSION "${GIT_VERSION}")

if(${GIT_RESULT})
	set(GIT_VERSION "NO_GIT_VERSION")
else()
	string(REGEX MATCHALL [0-9] HEADER_VERSION ${GIT_VERSION})
	list(GET HEADER_VERSION 0 RPI_JUKEBOX_PROTO_LIBS_VERSION_MAJOR)
	list(GET HEADER_VERSION 1 RPI_JUKEBOX_PROTO_LIBS_VERSION_MINOR)
	list(LENGTH HEADER_VERSION VER_LEN)
	if(${VER_LEN} LESS 3)
		set(RPI_JUKEBOX_PROTO_LIBS_VERSION_PATCH 0)
	else()
		list(GET HEADER_VERSION 2 RPI_JUKEBOX_PROTO_LIBS_VERSION_PATCH)	
	endif()
	set(COMPLETE_VERSION ${RPI_JUKEBOX_PROTO_LIBS_VERSION_MAJOR}.${RPI_JUKEBOX_PROTO_LIBS_VERSION_MINOR}.${RPI_JUKEBOX_PROTO_LIBS_VERSION_PATCH})
	message(STATUS "Git Version: ${GIT_VERSION}")
	message(STATUS "Version in header: ${COMPLETE_VERSION}")
endif()

set(INPUT_VERSION_HEADER_FILE ${CMAKE_CURRENT_SOURCE_DIR}/include/rpi_jukebox/api/proto_libs_version.h.in)
set(OUTPUT_VERSION_HEADER_FILE ${CMAKE_CURRENT_BINARY_DIR}/include/rpi_jukebox/api/proto_libs_version.h)
configure_file(${INPUT_VERSION_HEADER_FILE} ${OUTPUT_VERSION_HEADER_FILE})

include(build_steps.cmake)

if(NOT DEFINED ZIP_EXPORT_NAME)
	set(ZIP_EXPORT_NAME master_testing.zip)
endif()


add_custom_target(create_zip 
        DEPENDS ${PROTO_SRCS} ${PROTO_HDRS} ${PROTO_PY} export.zip
)

add_custom_command(OUTPUT export.zip
	COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/proto
	COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/tests
	COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/proto ${CMAKE_CURRENT_BINARY_DIR}/proto
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/tests ${CMAKE_CURRENT_BINARY_DIR}/tests
 	COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/build_steps.cmake ${CMAKE_CURRENT_BINARY_DIR}/
        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/CMakeLists.txt.in ${CMAKE_CURRENT_BINARY_DIR}/
	COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/cmake_download_googletest.txt ${CMAKE_CURRENT_BINARY_DIR}/
	COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/conanfile.py ${CMAKE_CURRENT_BINARY_DIR}/
	COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/LICENSE ${CMAKE_CURRENT_BINARY_DIR}/
	COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/CMakeLists.txt.export ${CMAKE_CURRENT_BINARY_DIR}/
	COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/include/rpi_jukebox/api/common.h ${CMAKE_CURRENT_BINARY_DIR}/include/rpi_jukebox/api/
	COMMAND ${CMAKE_COMMAND} -E rename  ${CMAKE_CURRENT_BINARY_DIR}/CMakeLists.txt.export ${CMAKE_CURRENT_BINARY_DIR}/CMakeLists.txt
	COMMAND ${CMAKE_COMMAND} -E tar "cfv" ${ZIP_EXPORT_NAME} --format=zip 
		 ${PROTO_SRCS} ${PROTO_HDRS} ${PROTO_PY} include
                 tests proto build_steps.cmake CMakeLists.txt.in cmake_download_googletest.txt conanfile.py LICENSE CMakeLists.txt)
