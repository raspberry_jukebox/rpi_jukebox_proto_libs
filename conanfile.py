#! /usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile
from conans import tools
from conans import CMake

class RpiJukeboxEventsConanFile(ConanFile):
    name = "RpiJukeboxEvents"
    license = "MIT"
    url = "https://gitlab.com/raspberry_jukebox/rpi_jukebox_proto_libs"
    description = "The events and RPCs, that can be used to communicate with the raspberry jukebox core application"
    generators = "cmake"
    settings = "os", "arch", "compiler", "build_type"
    options = {"testing" : [True, False], "shared" : [True, False]}
    default_options = {"testing" : True, "shared" : False}
    exports_sources = "include*", "proto*", "tests*", "*.txt*", "LICENSE", "build_steps.cmake"
    requires = "protobuf/3.6.1@bincrafters/stable"

    def build(self):
        cmake = CMake(self)
        cmake.definitions["TESTING"] = self.options.testing
        cmake.definitions["RPI_JUKEBOX_PROTO_LIBS_VERSION_MAJOR"] = self.version.split('.')[0]
        cmake.definitions["RPI_JUKEBOX_PROTO_LIBS_VERSION_MINOR"] = self.version.split('.')[1]
        cmake.definitions["RPI_JUKEBOX_PROTO_LIBS_VERSION_PATCH"] = self.version.split('.')[2]
        cmake.definitions["COMPLETE_VERSION"] = self.version
        cmake.definitions["RPI_JUKEBOX_EVENTS_DYN"] = self.options.shared
        cmake.configure()
        cmake.build()
        if self.options.testing:
            cmake.test()

    def build_requirements(self):
        if self.options.testing:
            self.build_requires("gtest/[>=1.8]@bincrafters/stable")
        self.build_requires("protoc_installer/3.6.1@realbigflo/stable")

    def package(self):
        self.copy("*.h", dst="include", src="include")
        self.copy("*pb.h", dst="include/rpi_jukebox/api/")
        self.copy("*rpi_jukebox_events.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["rpi_jukebox_events"]
