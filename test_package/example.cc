#include <gtest/gtest.h>
#include <string>
#include "rpi_jukebox/api/proto_libs_version.h"
#include "rpi_jukebox/api/common.h"
#include <iostream>

TEST(Version, NoGit) {
	ASSERT_NE(std::string(), raspberry_jukebox::api::Version::to_str());
	std::cerr << "Found version: " << raspberry_jukebox::api::Version::to_str() << std::endl;
}
