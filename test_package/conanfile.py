#! /usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile
from conans import CMake
import os

class RpiJukeboxEventsTestConanFile(ConanFile):
    generators = "cmake"
    settings = "os", "arch", "compiler", "build_type"
    requires = "gtest/[>=1.8]@bincrafters/stable", "RpiJukeboxEvents/[>=0.1]@realbigflo/release"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.dylib*", dst="bin", src="lib")

    def test(self):
        os.chdir("bin")
        self.run(".%sexample" % os.sep)