#include <gtest/gtest.h>
#include <string>
#include "rpi_jukebox/api/proto_libs_version.h"

TEST(Version, NoGit) {
	EXPECT_NE(std::string(), raspberry_jukebox::api::Version::to_str());
}
