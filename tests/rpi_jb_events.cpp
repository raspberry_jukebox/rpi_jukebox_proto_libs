// cppcheck-suppress missingInclude
#include "rpi_jukebox_events.pb.h"

#include <gtest/gtest.h>

TEST(EVENT, version) {
	ASSERT_NO_THROW(GOOGLE_PROTOBUF_VERIFY_VERSION);
}

TEST(EVENT, creation) {

	rpi_jukebox_events::Event event;
	EXPECT_EQ(event.uid(), std::string());
	event.set_uid("My Test UID");
	EXPECT_EQ(event.uid(), std::string("My Test UID"));
}
